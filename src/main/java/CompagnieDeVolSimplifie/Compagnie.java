package CompagnieDeVolSimplifie;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Compagnie {
    private String code;
    private String nom;
    private String siegeSocial;
    private List<Vol> vols;
    private List<Pilote> pilotes;

    public Compagnie(String code, String nom, String siegeSocial) {
        this.code = code;
        this.nom = nom;
        this.siegeSocial = siegeSocial;
        this.vols = new ArrayList<>();
        this.pilotes = new ArrayList<>();
    }

    public void reserverUnBillet(Vol vol, Avion avion) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nom du passager : ");
        String nomPassager = scanner.next();
        System.out.print("Prénom du passager : ");
        String prenomPassager = scanner.next();
        System.out.print("Date de naissance du passager : ");
        String dateNaissancePassager = scanner.next();
        System.out.print("Téléphone du passager : ");
        String telephonePassager = scanner.next();

        // Appel à la méthode reserverUnBillet de la classe Vol
        vol.reserverUnBillet(avion, nomPassager, prenomPassager, dateNaissancePassager, telephonePassager);
    }
    public void afficherInformationsVol(Vol vol) {
        vol.afficherInformationsVol();
    }
    public void afficherInformationsCompagnie() {
        System.out.println("Merci de voyager avec " + nom);
    }

    public void listeDesVols() {
        for (Vol vol : vols) {
            System.out.println(vol.toString());
        }
    }

    public void listeDesPilotes() {
        for (Pilote pilote : pilotes) {
            System.out.println(pilote.toString());
        }
    }
}
