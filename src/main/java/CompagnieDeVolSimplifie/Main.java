package CompagnieDeVolSimplifie;

public class Main {
    public static void main(String[] args) {
        // Création d'une compagnie
        Compagnie compagnie = new Compagnie("DTC", "Airkéké", "Boukistan");

        // Création d'un avion (Boeing 777 par défaut)
        Avion avion = new Avion("A001", "Boeing 777", 200, 200, 30);

        // Création d'un vol avec l'avion
        Vol vol = new Vol("222", "01/01/2023", "02/01/2023", "10:00", "12:00", "Paris", "New York", 0, avion);

        // Réservation d'un billet pour le vol avec l'avion
        compagnie.reserverUnBillet(vol, avion);

        // Affichage de la liste des vols
        compagnie.listeDesVols();

        // Affichage de la liste des pilotes
        compagnie.listeDesPilotes();

        // Affichage des informations du vol
        compagnie.afficherInformationsVol(vol);

        // Affichage des informations de la compagnie
        compagnie.afficherInformationsCompagnie();
    }
}