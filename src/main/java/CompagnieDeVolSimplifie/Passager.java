package CompagnieDeVolSimplifie;

public class Passager {
    private String nom;
    private String prenom;
    private String dateNaissance;
    private String telephone;

    public Passager(String nom, String prenom, String dateNaissance, String telephone) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.telephone = telephone;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    // Ajoutez les getters pour dateNaissance et telephone si nécessaire
}
