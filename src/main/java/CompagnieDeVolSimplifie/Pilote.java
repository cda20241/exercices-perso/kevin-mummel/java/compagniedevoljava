package CompagnieDeVolSimplifie;

public class Pilote {
    private String matricule;
    private String nom;
    private String prenom;
    private String qualification;

    public Pilote(String matricule, String nom, String prenom, String qualification) {
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.qualification = qualification;
    }
}