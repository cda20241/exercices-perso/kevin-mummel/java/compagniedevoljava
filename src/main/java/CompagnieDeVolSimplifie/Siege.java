package CompagnieDeVolSimplifie;

public class Siege {
    private String nom;
    private String nomAllee;
    private String nomRang;
    private String nomClasse;

    public Siege(String nomAllee, String nomRang, String nomClasse) {
        this.nomAllee = nomAllee;
        this.nomRang = nomRang;
        this.nom = nomAllee + nomRang;
        this.nomClasse = nomClasse;
    }

    public String getNom() {
        return nom;
    }

    public String getNomClasse() {
        return nomClasse;
    }

    @Override
    public String toString() {
        return nom + " (" + nomClasse + ")";
    }
}
