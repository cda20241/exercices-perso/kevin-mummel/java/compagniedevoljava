package CompagnieDeVolSimplifie;

public class Billet {
    private String nomBillet;
    private String nomPassager;
    private String prenomPassager;
    private String nomSiege;
    private String dateEmission;
    private String dateReservation;
    private String datePaiement;

    public Billet(String nomBillet, String nomPassager, String prenomPassager, String nomSiege,
                  String dateEmission, String dateReservation, String datePaiement) {
        this.nomBillet = nomBillet;
        this.nomPassager = nomPassager;
        this.prenomPassager = prenomPassager;
        this.nomSiege = nomSiege;
        this.dateEmission = dateEmission;
        this.dateReservation = dateReservation;
        this.datePaiement = datePaiement;
    }

    public void afficherNomPrenomPassager() {
        System.out.println("Nom: " + nomPassager + ", Prénom: " + prenomPassager);
    }
}
