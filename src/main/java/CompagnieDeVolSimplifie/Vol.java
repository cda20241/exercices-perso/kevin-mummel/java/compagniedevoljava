package CompagnieDeVolSimplifie;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Vol {
    private String numeroVol;
    private String dateDepart;
    private String dateArrivee;
    private String heureDepart;
    private String heureArrivee;
    private String villeDepart;
    private String villeArrivee;
    private int retard;
    private Avion avion;

    private List<Siege> sieges;

    public Vol(String numeroVol, String dateDepart, String dateArrivee, String heureDepart, String heureArrivee,
               String villeDepart, String villeArrivee, int retard, Avion avion) {
        this.numeroVol = numeroVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        this.retard = retard;
        this.avion = avion;
        this.sieges = genererSieges();
    }

    // Génére les sièges en fonction du model d'avion
    private List<Siege> genererSieges() {
        // Création d'une nouvelle liste pour stocker les sièges générés
        List<Siege> sieges = new ArrayList<>();

        //Compte le nombre de rangées dans l'objet "avion"
        int nombreRangees = avion.getNombreRangees();

        // Générer les sièges en fonction du nombre de rangées et d'allées
        for (int i = 1; i <= nombreRangees; i++) { // Incrémente la variable i à chaque boucle jusqu'à i > nombre de Rangées
            for (char j = 'A'; j < 'A' + avion.getNombreSieges() / nombreRangees; j++) { // Incrémente la variable j à chaque boucle jusqu'à j > nombre d'allées (Nbsiège/Nbrangées=Nballé)
                String nomClasse = (avion.getModelAvion().equals("Boeing 777") && i <= 10) ? "business" : "economique"; // si rangé < 10 business classe autrement économique
                sieges.add(new Siege(String.valueOf(j), String.valueOf(i), nomClasse)); // ajoute le siège a la valaeur j et i et nom de la classe
            }
        }
        return sieges;
    }

    public void afficherSiegesDisponibles() {
        System.out.println("Sièges disponibles : ");
        int nombreSiegesParRangee = avion.getNombreSieges() / avion.getNombreRangees(); // Compte le nombre d'allées
        int numeroRangeeActuel = 1; // initialise le numéro de rangée à 1

        System.out.print("Rangée " + numeroRangeeActuel + ": ");

        for (int i = 0; i < sieges.size(); i++) { // incrémente la variable i à chaque boucle jusqu'à i >= Nombre de sièges
            Siege siege = sieges.get(i); // Déclare la variable siège qui prend la valeur de la liste sièges à la position i
            System.out.print(siege.toString() + " ");

            if ((i + 1) % nombreSiegesParRangee == 0) { // Si i à la dernière valeur du nombre de sièges par rangée
                System.out.println();  // Saut de ligne pour chaque nouvelle rangée
                numeroRangeeActuel++; // Incrémente le numéro de rangée actuel
                if (numeroRangeeActuel <= avion.getNombreRangees()) { // Si le numéro de rangée actuel est plus petit ou égale au nombre de rangées de l'avion
                    System.out.print("Rangée " + numeroRangeeActuel + ": "); // Alors on affiche le numéro de rangée
                }
            }
        }
        System.out.println();
    }
    private void reserverSiege(Siege siege, Passager passager) {
        Billet billet = new Billet("B001", passager.getNom(), passager.getPrenom(), siege.getNom(),
                "01/01/2023", "01/01/2023", "01/01/2023");
        System.out.println("Siège " + siege.getNom() + " en classe " + siege.getNomClasse() + " réservé au Nom de " + passager.getNom() + " " + passager.getPrenom());
    }
    public void afficherInformationsVol() {
        System.out.println("Pour le Vol numéro " + numeroVol + " au départ de " + villeDepart + " le " + dateDepart + " à " + heureDepart + " et arrivée à " + villeArrivee + " le " + dateArrivee + " à " + heureArrivee);
    }

    public Siege trouverSiegeParNom(String nomSiege) {
        for (Siege siege : sieges) {
            if (siege.getNom().equals(nomSiege)) {
                return siege;
            }
        }
        return null;
    }


    public void reserverUnBillet(Avion avion, String nomPassager, String prenomPassager,
                                 String dateNaissancePassager, String telephonePassager) {
        boolean siegeInvalide = true;

        do {
            afficherSiegesDisponibles();
            Scanner scanner = new Scanner(System.in);
            System.out.print("Choisissez un siège disponible (ex. B23) : ");
            String choixSiege = scanner.next();
            Siege siegeChoisi = trouverSiegeParNom(choixSiege);

            if (siegeChoisi != null) {
                System.out.println("Billet réservé avec succès !");
                Passager passager = new Passager(nomPassager, prenomPassager, dateNaissancePassager, telephonePassager);
                reserverSiege(siegeChoisi, passager);
                siegeInvalide = false;  // Sortir de la boucle si le siège est réservé avec succès
            } else {
                System.out.print("Siège invalide ou déjà réservé. ");
            }
        } while (siegeInvalide);
    }
}
