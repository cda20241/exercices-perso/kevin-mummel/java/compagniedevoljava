package CompagnieDeVolSimplifie;

import java.util.ArrayList;
import java.util.List;

public class Avion {
    private String codeAvion;
    private String modelAvion;
    private int nombrePassager;
    private int nombreSieges;
    private int nombreRangees;

    public Avion(String codeAvion, String modelAvion, int nombrePassager, int nombreSieges, int nombreRangees) {
        this.codeAvion = codeAvion;
        this.modelAvion = modelAvion;
        this.nombrePassager = nombrePassager;
        this.nombreSieges = nombreSieges;
        this.nombreRangees = nombreRangees;
    }

    public int getNombreRangees() {
        return nombreRangees;
    }
    public int getNombreSieges() {
        return nombreSieges;
    }

    public String getModelAvion() {
        return modelAvion;
    }

    public List<Siege> genererSieges() {
        List<Siege> sieges = new ArrayList<>();

        int nombreRangees = getNombreRangees();

        for (int i = 1; i <= nombreRangees; i++) {
            for (char j = 'A'; j < 'A' + getNombreSieges() / nombreRangees; j++) {
                String nomClasse = (getModelAvion().equals("Boeing 777") && i <= 10) ? "business" : "economique";
                sieges.add(new Siege(String.valueOf(j), String.valueOf(i), nomClasse));
            }
        }

        return sieges;
    }
}
